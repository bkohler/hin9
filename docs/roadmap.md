
* bugs
  * wordpress fails in many places


* reverse proxy
  * cache
  * cache gzipped files


* cgi support
  * fastcgi


* daemon support
  * graceful restart
  * daemon detach from console
  * pid file file locking


* sandboxing
  * syscall filtering
  * capabilities
  * namespaces

